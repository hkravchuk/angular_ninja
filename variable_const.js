// object
console.log('object')
const PONY = {}; // {}
PONY.color = 'blue'; // { color: 'blue' }
PONY.color = 'red'; // { color: 'red' }
PONY.size = 'small'; // { color: 'red', size: 'small' }
console.log(PONY)

// object with values
console.log('object with values')
const PONY_2 = { color: 'blue' }; // { color: 'blue' }
PONY_2.color = 'red'; // { color: 'red' }
PONY_2.size = 'small'; // { color: 'red', size: 'small' }
console.log(PONY_2)

// list
console.log('list')
const PONIES = []; // []
PONIES.push({ color: 'blue' }); // [ { color: 'blue' } ]
PONIES[0].color = 'red'; // [ { color: 'red' } ]
PONIES.push({ size: 'small' }); // [ { color: 'red' }, { size: 'small' } ]
console.log(PONIES)

// list with values
console.log('list with values')
const PONIES_2 = [{ color: 'blue' }]; // [ { color: 'blue' } ]
PONIES_2[0].color = 'red'; // [ { color: 'red' } ]
PONIES_2.push({ size: 'small' }); // [ { color: 'red' }, { size: 'small' } ]
console.log(PONIES_2)
try {
    PONIES_2 = [];
}
catch (e) {
    console.log(e) // TypeError: Assignment to constant variable.
}